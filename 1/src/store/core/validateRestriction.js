const _ = require('underscore');

export function validateRestriction(arraySky, arrayStore, currentCountry) {
  var clonedArrStore = arrayStore,
      objUpdate = [],
      html = '';

  if (_.isArray(arraySky) && arraySky.length > 0) {        
    html += '<div>';
    html += '<br />';
    html += ' <p class="restricted-msg">' + 'The following products will be removed from your shopping cart, they are' + '<strong>' + ' restricted in ' + currentCountry + '</strong>';
    html += ' <table>';
    html += '   <tbody>';

    arraySky.map((item) => {
      for (var i in clonedArrStore) {
        var objStore = clonedArrStore[i],
            code = objStore.quote_id;

        if ((item.HtmlObjectId).trim() === (code).trim()) {       
          objUpdate.push(objStore.variant_id);

          html += '     <tr>';
          html += '       <td>';
          html += '         <center><img style="height: 150px;" src="' + objStore.image + '"</img>' + '</center>';
          html += '       </td>';
          html += '       <td>';
          html += '         <strong> <p class="restricted-msg">' + objStore.product_title + ' - '  + objStore.variant_title  + '</p>' + '</strong>' + '<br />';
          html += '       </td>';
          html += '     </tr>';
          break;
        }
      }
    });

    html += '   </tbody>';
    html += ' </table>';
    html += '</div>';
  }

  return {
    objUpdate: objUpdate,
    html: html 
  }
}