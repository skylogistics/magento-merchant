const _ = require('underscore');
const arrayDiff = require('simple-array-diff');
const tingle = require('tingle.js');
// Storage
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]
};
const sessionStore = engine.createStore(storage.session, []);
const localStore = engine.createStore(storage.local, []);
// Local
const api = require('../../xhr');
const __cnStore = require('../../../config/store.json');

import { showLoaders } from "./showLoaders.js";
import { updateShoppingCart_hrml } from './updateShoppingCart_hrml.js';
import { addProductItems } from './addProductItems.js';
import { validateRestriction } from './validateRestriction.js';

export function synchronizeCart(data) {
  if (window.stateXhr == 1) {
    window.stateXhr++;

    if (data.length > 0) {
      var itemsStore = JSON.parse(data),
          arrayRestricted = [],
          operations = [];

      Sdk.getLocationAllow().then(function () {
        var Cart = sessionStore.get('cart_prod_arr');
        var itemsSky = (!_.isUndefined(Cart) && !_.isNull(Cart)) ? Cart.Data.Cart.Items : [];

        if (itemsStore.length > 0) {
          var lastCountry = sessionStore.get('last_country').toUpperCase().trim();
          var currentCountry = (sessionStore.get('auth-store').Data.CART_SKY.Country.Name).toUpperCase().trim();

          if (currentCountry !== lastCountry) {
            var loaderModal = new tingle.modal({
              footer: false,
              stickyFooter: false,
              closeMethods: []
            });
            loaderModal.setContent('<center><span> Wait a moment please </span><div style="padding-top: 10px;"><img src="https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderBlackBlue.gif"  /></div></center>');
            loaderModal.open();

            Sdk.getCartRefresh().then(function (Cart) {
              itemsSky = (!_.isUndefined(Cart) && !_.isNull(Cart)) ? Cart.Data.Cart.Items : [];

              var restrictedModal = new tingle.modal({
                footer: true,
                stickyFooter: true,
                closeMethods: ['overlay', 'button', 'escape'],
                closeLabel: "Close",
                onClose: function () {
                  setTimeout(function () {
                    top.location.reload();
                  }, 500);
                }
              });

              if (_.isArray(itemsSky) && itemsSky.length > 0) {
                arrayRestricted = _.filter(itemsSky, function (prod) {
                  var restricted = prod.IsRestricted;
                  return restricted === true;
                });

                for (let i = 0; i < itemsStore.length; i++) {
                  itemsStore[i].Code = String(itemsStore[i].variant_id);
                  itemsStore[i].Quantity = String(itemsStore[i].quantity);
                }

                var result = arrayDiff(itemsSky, itemsStore, 'Code');

                var cart_prod_arr_add = result.added,
                  cart_prod_arr_remove = result.removed,
                  cart_prod_arr_common = result.common;

                if (cart_prod_arr_add.length > 0) {
                  var prod = addProductItems(cart_prod_arr_add);
                  operations.push(Sdk.addProductsCart(prod));
                }

                if (cart_prod_arr_remove.length > 0) {
                  for (let a = 0; a < cart_prod_arr_remove.length; a++) {
                    let id = cart_prod_arr_remove[a].Id;
                    operations.push(Sdk.removeProductCart(id));
                  }
                }

                if (arrayRestricted.length > 0) {
                  for (let a = 0; a < arrayRestricted.length; a++) {
                    let id = arrayRestricted[a].Id;
                    operations.push(Sdk.removeProductCart(id));
                  }
                }

                var array_diff = [];

                if (cart_prod_arr_common.length > 0) {
                  for (let a = 0; a < cart_prod_arr_common.length; a++) {
                    let itemStore = cart_prod_arr_common[a];
                    for (let b = 0; b < itemsSky.length; b++) {
                      let itemSky = itemsSky[b];

                      if (itemStore.Code === itemSky.Code) {
                        if ((itemStore.Quantity).toString() !== itemSky.Quantity.toString()) {
                          array_diff.push({
                            Id: itemSky.Id,
                            Quantity: itemStore.Quantity
                          });
                        }
                      }
                    }
                  }

                  if (array_diff.length > 0) {
                    let Products = { 'Product': array_diff };
                    operations.push(Sdk.editProductsCart(Products));
                  }
                }

                Promise.all(operations).then(function (response) {
                  showLoaders();
                  var productsResponse = (response[0] && response[0].Data) ? response[0].Data.ListProducts : [];

                  arrayRestricted.map((item) => {
                    productsResponse.push({
                      Success: false,
                      HtmlObjectId: String(item.Code),
                      Id: item.Id
                    });
                  });

                  var arrayRemove = _.filter(productsResponse, function (prod) {
                    var success = prod.Success;
                    return success === false;
                  });

                  if (arrayRemove.length < 1) {
                    Sdk.getCartRefresh().then(function (Cart2) {
                      sessionStore.set('cart_prod_arr', []);
                      sessionStore.set('cart_prod_arr', Cart2);
                      Sdk.Common().elementVisibleStore(false);
                      Sdk.Common().changeLastCountry(sessionStore.get('auth-store').Data.CART_SKY.Country.Name);
                      updateShoppingCart_hrml(Cart2);
                      Sdk.getConcepts().then(function (html) {
                        let htmlCart = Sdk.Common().conceptsTableCart(html);
                        $('.skbx-loader-subtotal').hide();
                        $('.international-checkout').html(htmlCart);
                        Sdk.Common().initBtnSkyCheckout();
                        window.stateXhr = 0;
                        loaderModal.close();
                      });
                    });
                  }
                  else {
                    var validate = validateRestriction(arrayRemove, itemsStore, currentCountry),
                      update = validate.objUpdate,
                      html = validate.html;

                    if (Object.keys(update).length > 0) {
                      deleteProductStore({}, update).then(function () {
                        if (html.length > 0) {
                          localStore.remove('mage-cache-storage');
                          restrictedModal.setContent(html);
                          restrictedModal.addFooterBtn('Close', 'tingle-btn tingle-btn--default tingle-btn--pull-right', function () {
                            restrictedModal.close();
                          });
                        }

                        Sdk.getCartRefresh().then(function (Cart2) {
                          sessionStore.set('cart_prod_arr', []);
                          sessionStore.set('cart_prod_arr', Cart2);
                          Sdk.Common().changeLastCountry(sessionStore.get('auth-store').Data.CART_SKY.Country.Name);
                          updateShoppingCart_hrml(Cart2);
                          Sdk.getConcepts().then(function (html) {
                            let htmlCart = Sdk.Common().conceptsTableCart(html);
                            $('.skbx-loader-subtotal').hide();
                            $('.international-checkout').html(htmlCart);
                            Sdk.Common().initBtnSkyCheckout();
                            window.stateXhr = 0;
                            loaderModal.close();
                            restrictedModal.open();
                          });
                        });
                      });
                    }
                  }
                });
              }
              else {
                let prod = addProductItems(itemsStore);

                Sdk.addProductsCart(prod).then(function (response) {
                  var productsResponse = response.Data ? response.Data.ListProducts : [];
                  var arrayRemove = _.filter(productsResponse, function (prod) {
                    var success = prod.Success;
                    return success === false;
                  });

                  if (arrayRemove.length < 1) {
                    Sdk.getCartRefresh().then(function (Cart2) {
                      sessionStore.set('cart_prod_arr', []);
                      sessionStore.set('cart_prod_arr', Cart2);
                      Sdk.Common().changeLastCountry(sessionStore.get('auth-store').Data.CART_SKY.Country.Name);
                      updateShoppingCart_hrml(Cart2);
                      Sdk.getConcepts().then(function (html) {
                        let htmlCart = Sdk.Common().conceptsTableCart(html);
                        $('.skbx-loader-subtotal').hide();
                        $('.international-checkout').html(htmlCart);
                        Sdk.Common().initBtnSkyCheckout();
                        window.stateXhr = 0;
                        loaderModal.close();
                      });
                    });
                  }
                  else {
                    var validate = validateRestriction(arrayRemove, itemsStore, currentCountry),
                      update = validate.objUpdate,
                      html = validate.html;

                    let params = {
                      idVariant: update
                    };

                    if (Object.keys(update).length > 0) {
                      deleteProductStore(response).then(function () {
                        if (html.length > 0) {
                          restrictedModal.setContent(html);
                          restrictedModal.addFooterBtn('Close', 'tingle-btn tingle-btn--default tingle-btn--pull-right', function () {
                            restrictedModal.close();
                          });
                        }

                        Sdk.getCartRefresh().then(function (cart_) {
                          sessionStore.set('cart_prod_arr', []);
                          sessionStore.set('cart_prod_arr', cart_);
                          Sdk.Common().changeLastCountry(sessionStore.get('auth-store').Data.CART_SKY.Country.Name);
                          updateShoppingCart_hrml(cart_);
                          Sdk.getConcepts().then(function (html) {
                            let htmlCart = Sdk.Common().conceptsTableCart(html);
                            $('.skbx-loader-subtotal').hide();
                            $('.international-checkout').html(htmlCart);
                            Sdk.Common().initBtnSkyCheckout();
                            window.stateXhr = 0;
                            loaderModal.close();
                            restrictedModal.open();
                          });
                        });
                      });
                    }
                  }
                });
              }
            });
          } else {
            if (_.isArray(itemsSky) && itemsSky.length > 0) {
              for (let i = 0; i < itemsStore.length; i++) {
                itemsStore[i].Code = String(itemsStore[i].variant_id);
                itemsStore[i].Quantity = String(itemsStore[i].quantity);
              }

              var result = arrayDiff(itemsSky, itemsStore, 'Code');

              var cart_prod_arr_add = result.added,
                cart_prod_arr_remove = result.removed,
                cart_prod_arr_common = result.common;

              if (cart_prod_arr_add.length > 0) {
                var prod = addProductItems(cart_prod_arr_add);
                operations.push(Sdk.addProductsCart(prod));
              }

              if (cart_prod_arr_remove.length > 0) {
                for (let a = 0; a < cart_prod_arr_remove.length; a++) {
                  let id = cart_prod_arr_remove[a].Id;
                  operations.push(Sdk.removeProductCart(id));
                }
              }

              if (cart_prod_arr_common.length > 0) {
                var array_diff = [];

                for (let a = 0; a < cart_prod_arr_common.length; a++) {
                  let itemStore = cart_prod_arr_common[a];
                  for (let b = 0; b < itemsSky.length; b++) {
                    let itemSky = itemsSky[b];

                    if (itemStore.Code === itemSky.Code) {
                      if ((itemStore.Quantity).toString() !== itemSky.Quantity.toString()) {
                        array_diff.push({
                          Id: itemSky.Id,
                          Quantity: itemStore.Quantity
                        });
                      }
                    }
                  }
                }

                if (array_diff.length > 0) {
                  let Products = { 'Product': array_diff };
                  operations.push(Sdk.editProductsCart(Products));
                }
              }

              Promise.all(operations).then(function (response) {
                var productsResponse;

                if (response.length > 0) {
                  if (_.isArray(response))
                    productsResponse = (response && response[0].Data) ? response[0].Data.ListProducts : [];
                  else
                    productsResponse = (response && response.Data) ? response.Data.ListProducts : [];

                  var arrayRemove = _.filter(productsResponse, function (prod) {
                    var success = prod.Success;
                    return success === false;
                  });

                  if (arrayRemove.length > 0) {
                    deleteProductStore(response).then(function () {
                      localStore.remove('mage-cache-storage');
                      window.stateXhr = 0;
                      location.reload();
                    });
                  }
                }

                showLoaders();
                Sdk.getCartRefresh().then(function (Cart2) {
                  sessionStore.set('cart_prod_arr', []);
                  sessionStore.set('cart_prod_arr', Cart2);
                  updateShoppingCart_hrml(Cart2);
                  Sdk.getConcepts().then(function (html) {
                    let htmlCart = Sdk.Common().conceptsTableCart(html);
                    $('.skbx-loader-subtotal').hide();
                    $('.international-checkout').html(htmlCart);
                    Sdk.Common().initBtnSkyCheckout();
                    window.stateXhr = 0;
                  });
                });
              }).catch(function (error) {
                console.log(':: synchronizeCart --> Promise.all(operations) error', error);
              });
            }
            else {
              let prod = addProductItems(itemsStore);
              Sdk.addProductsCart(prod).then(function (response) {
                var productsResponse = (response && response.Data) ? response.Data.ListProducts : [];
                var arrayRemove = _.filter(productsResponse, function (prod) {
                  var success = prod.Success;
                  return success === false;
                });

                if (arrayRemove.length > 0) {
                  deleteProductStore(response).then(function () {
                    localStore.remove('mage-cache-storage');
                    window.stateXhr = 0;
                    location.reload();
                  });
                }

                Sdk.getCartRefresh().then(function (Cart2) {
                  sessionStore.set('cart_prod_arr', []);
                  sessionStore.set('cart_prod_arr', Cart2);
                  updateShoppingCart_hrml(Cart2);
                  Sdk.getConcepts().then(function (html) {
                    let htmlCart = Sdk.Common().conceptsTableCart(html);
                    $('.skbx-loader-subtotal').hide();
                    $('.international-checkout').html(htmlCart);
                    Sdk.Common().initBtnSkyCheckout();
                    window.stateXhr = 0;
                  });
                });
              });
            }
          }
        }
        else {
          if (itemsSky.length) {
            Sdk.deleteProductsCart().then(function (res) {
              sessionStore.remove('cart_prod_arr');
              window.stateXhr = 0;
            });
          } else {
            sessionStore.remove('cart_prod_arr');
            window.stateXhr = 0;
          }
          //When magento shopping cart expire. Clear mini cart cache(GF)
          var mageCacheStorage = localStore.get('mage-cache-storage');
          if(_.isObject(mageCacheStorage) && mageCacheStorage.cart.summary_count > 0){
            localStore.remove('mage-cache-storage');
            location.reload();
          }          
        }
      }).catch(function (error) {
        console.log(':: getLocationAllow error ', error);
      });
    }
  }
}

function deleteProductStore(response, arrayRestricted) {
  return new Promise(function (resolve, reject) {
    var productsResponse = '';
    if (_.isArray(response)) {
      if (_.isArray(response))
        productsResponse = (response && response[0].Data) ? response[0].Data.ListProducts : [];
      else
        productsResponse = (response && response.Data) ? response.Data.ListProducts : [];

      var arrayRemove = _.filter(productsResponse, function (prod) {
        var success = prod.Success;
        return success === false;
      });

      let idV = [];
      if (arrayRemove.length > 0) {

        for (let i = 0; i < arrayRemove.length; i++) {
          if (arrayRemove[i].Errors.length > 0) {
            idV.push({
              idVariant: arrayRemove[i].Product.Sku,
              Errors: arrayRemove[i].Errors
            });
          }
        }
        return resolve(api.get(__cnStore.STORE_GET_CART_MAGENTO + '/delete?idVariant=' + JSON.stringify(idV)));
      }
    }
    else {
      let idV = [];
      let errors = [];
      errors.push({
        'Code': 50032
      });
      for (let i = 0; i < arrayRestricted.length; i++) {
        idV.push({
          idVariant: arrayRestricted[i],
          Errors: errors
        });
      }
      console.log(idV);
      return resolve(api.get(__cnStore.STORE_GET_CART_MAGENTO + '/delete?idVariant=' + JSON.stringify(idV)));
    }
  });
}