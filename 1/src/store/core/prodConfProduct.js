import { multipleCalculate } from './multicalculate.js';

export function prodConfProduct() {

    var parentId = 0;
    window.lastParentId = 0;
    var sPrefixSelectsClassName = [];

    /***********CUSTOM SECTION************/
    //prefix class name combobox
    sPrefixSelectsClassName[0] = '';

    if(!window.onCatalog){
        parentId = parseInt(document.getElementsByName("product")[0].getAttribute("value"));
    }
    /**************************************/
    
    for (let iSelect = 0; iSelect < sPrefixSelectsClassName.length; iSelect++) {
        var sPrefixSelectClassName = sPrefixSelectsClassName[iSelect];
        if(sPrefixSelectClassName.length > 0){
            var selectInterval = setInterval(function(){
                $("[class*='" + sPrefixSelectClassName + "']").get().forEach(function(entry, index, array) {
                    if(array[index].tagName.match('SELECT')){
                        if(isOptionWraper(array[index])){
                            array[index].addEventListener("change", function() {
                                /*******************CUSTOM***************** */
                                if (iSelect == 0) {
                                    attId       = array[index].getAttribute("option-id");
                                    attValue    = array[index].parentNode.parentNode.parentNode.attr('attribute-id');
                                }
                                /******************************************** */
                                pushOptionId(attId, attValue,parentId);
                            });

                            clearInterval(selectInterval);
                        }
                    }
                });
            }, 200);
        }
    }

    /***********CUSTOM SECTION************/
    //prefix class swatch color
    var sPrefixColorSwatchClass = '';
    var sAttAttributeName       = ''; 
    var sOptionAttributeName    = '';
    var sClassParentDelimit     = '';
    /**************************************/

    if(sPrefixColorSwatchClass.length > 0){
        var swatchColorInterval = setInterval(function(){
            $("[class*='" + sPrefixColorSwatchClass + "']").get().forEach(function(entry, index, array) {                            
                if(window.onCatalog){
                    if(array[index].tagName.match('DIV') && $(array[index]).parents(sClassParentDelimit).length > 0){
                        listeningDivSwatch(array[index]);
                        clearInterval(swatchColorInterval);
                    }
                }else{
                    if(array[index].tagName.match('DIV')){
                        if(isOptionWraper(array[index])){                                    
                            listeningDivSwatch(array[index]);
                            clearInterval(swatchColorInterval);
                        }                                    
                    }
                }
            });
        }, 200);
        
        function listeningDivSwatch(node){
            node.addEventListener("click", function() {                
                /***********CUSTOM SECTION************/
                var optId       = node.getAttribute(sOptionAttributeName);
                var attId       = node.parentNode.parentNode.getAttribute(sAttAttributeName);
                
                if(window.onCatalog){
                    parentId = parseInt(node.parentNode.parentNode.parentNode.className.split('-')[2]);
                }
                /**************************************/
                pushOptionId(attId, optId, parentId);
            });
        }
    }

    var selected_attributes = {};

    function pushOptionId(attId, optId, parentId) {

        if(window.lastParentId != parentId){
            selected_attributes = {};
        }

        window.lastParentId = parentId;

        if(typeof selected_attributes[idAttr] == 'undefined'){
            selected_attributes[idAttr] = [];
        }
        selected_attributes[idAttr] = optId;

        var attrIdsSelected     = Object.keys(selected_attributes);
        var idProducts          = Object.keys(window.optionsByProduct[parentId]);
        var productsFiltered    = [];
        
        var product_found = true;

        if(attrIdsSelected.length == window.optionsByProduct[parentId].total_att){
            //CASE ALL ATTRIBUTES SELECTEDS, THEN THE PRICE CAN CHANGE

            for (let iProduct = 0; iProduct < idProducts.length - 1; iProduct++) {

                var idProduct           = idProducts[iProduct];
                var attrIdsByProduct    = Object.keys(window.optionsByProduct[parentId][idProduct]);
                
                product_found = true;

                for (let iAttProduct = 0; iAttProduct < attrIdsByProduct.length; iAttProduct++) {
                    var idAttributeProduct = attrIdsByProduct[iAttProduct]

                    var optValueProduct     = window.optionsByProduct[parentId][idProduct][idAttributeProduct]
                    var optValueSelected    = selected_attributes[idAttributeProduct];                                    
                        
                    if( optValueSelected != optValueProduct ){
                        product_found   = false;
                        break;
                    }
                }

                if(product_found){
                    //SHOW THE PRICE SELECTED                                
                    for (let iProductToHide = 0; iProductToHide < idProducts.length - 1; iProductToHide++) {
                        $("[id='skybox-product-price-" + idProducts[iProductToHide] + "']").parent().hide();
                    }
                    if(window.onCatalog){
                        var id = "skybox--product--price--" + idProduct;
                        if($("#" + id).length>0){
                            $("#" + id).attr('class', 'internationalPrice');
                            $("#" + id).attr('id', "skybox-product-price-" + idProduct);
                            multipleCalculate();
                        }
                        ;
                        $("#skybox-product-price-" + idProduct).parent().show();
                        
                    }else{
                        $("#skybox-product-price-" + idProduct).parent().show();
                        if ( $('#skybox-product-price-' + idProduct).children('.not-available').length == 1 ) {
                            $(".Sky--btn-add").hide();
                       }else{
                            $(".Sky--btn-add").show();
                       }
                    }
                    break
                }
            }

            if(product_found){
                //CASE PARENT PRODUCT MUST BE SHOW OR HIDE
                document.getElementById("skybox-product-price-" + parentId.toString()).setAttribute("style","display:none");                
            }else{
                $("#skybox-product-price-" + parentId.toString()).parent().show();
            }
        }
        console.log(selected_attributes);
    }

    function isOptionWraper(child) {
        var node = child.parentNode;
        while (node != null) {
            if(typeof node.className != 'undefined'){
                if (node.className.indexOf('product-options-wrapper') >=0){
                    return true;
                }
                node = node.parentNode;
            }else{
                break;
            }
        }
        return false;
    }
}