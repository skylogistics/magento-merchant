import { showLoaders } from "./showLoaders";
const _ = require('underscore');
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]
};
const sessionStore = engine.createStore(storage.session, []);
const localStore = engine.createStore(storage.local, []);
// Local
const api = require('../../xhr');
const __cnStore = require('../../../config/store.json');

function showPriceMultiC(productId, dataResp) {
  var prefModal = ($(".remodal-is-opened").length > 0) ? ".js-quick-shop " : "";// only for modal
  $(prefModal + '#skybox-product-price-' + productId).html(dataResp.responseText);
  $(prefModal + '#skybox-product-price-' + productId).removeClass('empty-price');
  $(".skbx-loader-" + productId).hide();
}


String.prototype.escapeSpecialChars = function () {
  return this.replace(/%27/g, "")
};

export function multipleCalculate() {
  //$('.price-box.price-final_price').addClass('skbx-price-store');

  Sdk.getLocationAllow().then(function () {
    showLoaders();
    Sdk.getAuthCart().then(function (store) {
      let ListProducts_Array = [],
        ListProducts = {};

      try {
        $(".internationalPrice").each((i, element) => {
          let data = String(element.getAttribute('data')).escapeSpecialChars();
          let elementId = element.getAttribute('id'),
              product = JSON.parse(data);

          if (elementId && element.childElementCount == 0 && product) {
            let id_shopify_product_variant = product.variantID,
              title = product.name,
              price = parseFloat(product.price),
              images = product.image,
              variantID = product.variantID,
              product_type = product.category;


            if (__cnStore.CONF.PRODUCT_TYPE_DEFAULT !== '') {
              product_type = __cnStore.CONF.PRODUCT_TYPE_DEFAULT;
            }

            var _HtmlObjectid = product.variantID + '__' + Sdk.Common().getDate() + store.Data.CART_SKY.Country.Iso + store.Data.CART_SKY.Cart.Currency;

            let _Optionals = {
              CustomFields: [
                product.sku
              ]
            };

            var storeConfiguration = localStore.get('skb-cfg');

            if (product_type !== '') {
              ListProducts_Array.push(
                {
                  HtmlObjectid: _HtmlObjectid,
                  Sku: variantID,
                  Name: title,
                  Category: product_type,
                  Price: price,
                  ImgUrl: images,
                  Weight: (product.weight === '' || product.weight === null) ? 0 : product.weight,
                  WeightUnit: storeConfiguration.weightUnit,
                  Optionals: _Optionals
                }
              );
            }
            else {
              console.log('::MULTIPLECALCULATE DATO FALTANTE CATEGORY', id_shopify_product_variant + ',' + product_type);
              $(".skbx-loader-" + id_shopify_product_variant).html('<span>Product not available in your country</span>');
              $('.Sky--btn-add').hide();
            }
          }
        })
      } catch (e) {
        console.log(e)
      }
      ListProducts_Array = _.uniq(ListProducts_Array, false, function (pred) {
        return pred.HtmlObjectid;
      });

      ListProducts = { ListProducts: ListProducts_Array };

      if (ListProducts_Array.length > 0) {
        Sdk.getMulticalculate(ListProducts).then(function (urlHtml) {
          // add new attribute: success [state]
          urlHtml.Data.forEach(element => {
            element.success = false;
          });

          var count = 0;
          var showPriceTimer = setInterval(function () {
            getHtmlMultipleCalculate(urlHtml);
            // Verify all success states from every urlHTML object
            urlHtml.Data.forEach(element => {
              if (element.success) {
                count += 1;
              }
            });
            // Kill setInterval
            if (count == urlHtml.Data.length) {
              clearInterval(showPriceTimer);
            }
          }, 1000);
        }).catch(function (error) {
          console.log(error);
          $("span[class^='skbx-price']").hide();
        });
      }

      function getHtmlMultipleCalculate(res, array) {
        let productId = "";
        let url = "";
        let __Res = [];

        for (let i = 0; i < res.Data.length; i++) {
          productId = res.Data[i].HtmlObjectId.toString().split('__')[0];
          url = res.Data[i].Url;
          (function (productId, url, i) {
            //Verify if the success status is false
            var x = productId;
            if (!res.Data[i].success) {
              return api.get(url).then(function (dataResp) {
                //restricted product
                if (dataResp.responseText.length > 1) {
                  showPriceMultiC(productId, dataResp);
                  if ($('#' + 'skybox-product-price-' + productId).parent().is(':visible')) {
                    if ($(dataResp.responseText).is('span.not-available')) {
                      $('#' + 'skybox-product-price-' + productId).parents('.product-item-details').find('.Sky--btn-add').hide();
                      $('#' + 'skybox-product-price-' + productId).parents('.product-info-main').find('.Sky--btn-add').hide();
                    } else {
                      $('#' + 'skybox-product-price-' + productId).parents('.product-item-details').find('.Sky--btn-add').show();
                      $('#' + 'skybox-product-price-' + productId).parents('.product-info-main').find('.Sky--btn-add').show();
                    }
                  }

                  $('#' + 'skybox-product-price-' + productId).html(dataResp.responseText);
                  $(".skbx-loader-" + productId).hide();
                  // Change the success status to avoid another request
                  res.Data[i].success = true;
                }
              }).catch(function (error) {
                console.log('getHtmlMultipleCalculate2', error);
              });
            }
          })(productId, url, i);
        }
      }
    });
  }).catch(function (error) {
    console.warn('LocationAllow::', error);
  });
}