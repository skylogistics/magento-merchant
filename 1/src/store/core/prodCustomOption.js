import { multipleCalculate } from './multicalculate.js';

export function prodCustomOption() {
    
    window.changeOnCustomOption = false;
    window.lastPrice = getCurrentPrice();

    //EVENT ON CHANGE PRICE    
    var price_wrapper = document.getElementsByClassName("price-wrapper");

    var config = { childList: true };

    var callback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
                var newPrice = getCurrentPrice();
                if(window.lastPrice != newPrice){
                    //console.log('Price change detected');
                    //GET All Data of original product                    
                    var productTag = document.getElementById("skybox-product-price-" + window.productParentId);
                    if(productTag){                        
                        var data            = JSON.parse(productTag.getAttribute("data"));
                        data.price          = newPrice;                        
                        window.lastPrice    = newPrice;

                        productTag.setAttribute("data", JSON.stringify(data));

                        //SHOW LOADER
                        $('.skbx-loader-' + window.productParentId).show();

                        //CLEAN SKYBOX PRICE CONTENT
                        $('#skybox-product-price-' + window.productParentId).empty();

                        //CALL MULTICALCULATE
                        multipleCalculate();
                    }
                }                
            }
        }
    };

    var observer = new MutationObserver(callback);

    observer.observe(price_wrapper[0], config);

    function getCurrentPrice(){

        var tagPrice = document.getElementById("product-price-" + window.productParentId);
        var tagPrices = tagPrice.childNodes;
        var arrayStringPrice = ["0", "0"];
        if(tagPrices.length > 0){
            for (let iPrices = 0; iPrices < tagPrices.length; iPrices++) {                
                if(tagPrices[iPrices].className == "price"){
                    arrayStringPrice = tagPrices[iPrices].textContent.match(/\d+/g);
                }
                
            }
        }        
        
        var sNumber = '';
        for (let iIntPart = 0; iIntPart < arrayStringPrice.length; iIntPart++) {
            if(arrayStringPrice.length -1 == iIntPart){
                sNumber += "." + arrayStringPrice[iIntPart];
            }else{
                sNumber += arrayStringPrice[iIntPart];
            }
            
        }
        return Number(sNumber);
    }
}