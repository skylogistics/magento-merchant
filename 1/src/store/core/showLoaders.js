let ruta = 'https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/';
let rutaLoaderGif = ruta + 'Images/loaders/loaderBlackBlue.gif';

export function showLoaders() {
  $("[class*=skbx-loader]").html(`
    <div>
      <img style="height: 15px;width: 100px !important" src="${rutaLoaderGif}" />
    </div>
  `);
}