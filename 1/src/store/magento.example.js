import { setTimeout } from 'timers';
import SkyboxSDK from '@skyboxcheckout/merchant-sdk';

const _ = require('underscore');
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]
};
const localStore = engine.createStore(storage.local, []);
const sessionStore = engine.createStore(storage.session, []);
// Local libs
const magentoStyles = require('../../assets/css/magento.css');
const __cnStore = require('../../config/store.json');
const api = require('../xhr');

import { multipleCalculate } from './core/multicalculate.js';
import { synchronizeCart } from './core/synchronizeCart.js';
import { prodCustomOption } from './core/prodCustomOption.js';
import { prodConfProduct } from './core/prodConfProduct.js';


let conf = localStore.get('skb-cfg');

if (_.isObject(CONF)) {
  __cnStore.IDSTORE = conf.merchantId;
  __cnStore.MERCHANTCODE = conf.merchantCode;
  __cnStore.MERCHANT = conf.merchantKey;
  __cnStore.SUCCESSFUL_PAGE = conf.successUrl;
  __cnStore.CHECKOUT_PAGE = conf.checkoutUrl;
  __cnStore.STORE_GET_CART_MAGENTO = conf.cartUrl

  if (!_.isNull(conf.weightUnit) && !_.isUndefined(conf.weightUnit) && conf.weightUnit !== "") {
    window.Sdk = new SkyboxSDK({
      IDSTORE: __cnStore.IDSTORE,
      MERCHANT: __cnStore.MERCHANT,
      MERCHANTCODE: __cnStore.MERCHANTCODE,
      STORE_URL: __cnStore.STORE_URL,
      SUCCESSFUL_PAGE: __cnStore.SUCCESSFUL_PAGE,
      CHECKOUT_PAGE: __cnStore.CHECKOUT_PAGE,
      CHECKOUT_BUTTON_CLASS: __cnStore.CHECKOUT_BUTTON_CLASS
    });
  }
}

$(document).ready(function () {
  window.stateXhr = 0;
  Sdk.Common().initChangeCountry();

  if (window.customOptionsUsed) {
    prodCustomOption();
  }

  if (window.confProductUsed) {
    prodConfProduct();
  }

  setTimeout(() => {
    multipleCalculate();

    api.get(__cnStore.STORE_GET_CART_MAGENTO+'?i='+Math.floor((Math.random() * 9999) + 1000)).then((response) => {
      window.stateXhr++;
      synchronizeCart(response.responseText);
    });
  }, 1000);

  // Skybox Ckeckout page & Iframe Source
  if ($(location).attr('href').indexOf(__cnStore.CHECKOUT_PAGE) > -1 &&
    $(location).attr('href').indexOf(__cnStore.SUCCESSFUL_PAGE) === -1) {
    $('#skybox-checkout-change-country').css('display', 'none');
    $("#skybox-international-checkout").css('display', 'none');
    /* hide minicart icon
      $('.minicart-wrapper').css('display', 'none');
    */
    var pOptional = {
      IdCartExternal: "" ,
      IdStoreExternal: localStore.get('skb-cfg').IdStore
    };

    Sdk.showCheckoutPageV2(pOptional);
  }

  // Skybox Ckeckout Successfull page & Invoice Source
  if ($(location).attr('href').indexOf(__cnStore.SUCCESSFUL_PAGE) > -1) {
    $('#skybox-checkout-change-country').css('display', 'none');
    /* hide minicart icon
      $('.minicart-wrapper').css('display', 'none');
    */
    if ($('#skybox-international-checkout-invoice').length > 0) {
      api.get(__cnStore.STORE_GET_CART_MAGENTO + '/clear').then(() => {
        localStore.remove('mage-cache-storage');
        var rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderGris.gif";

        if (__cnStore.SKBX_LOADER_NAME) {
          rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/" + __cnStore.SKBX_LOADER_NAME;
        }
        
        $('#skybox-international-checkout-invoice').html(`
          <h1 id="mensaje" style="text-align:center;">
            <center><img src="${rutaLoaderGif}"/></center>
          </h1>
        `);

        Sdk.getCartInvoice().then(function (content) {
          var contentHtml = JSON.parse(content).Data.Invoice;
          var invoice = document.getElementById('skybox-international-checkout-invoice');
          invoice.innerHTML = contentHtml;
        });
      });
    }
  }

  // Get Event Listener
  (function (open) {
    XMLHttpRequest.prototype.open = function () {
      this.addEventListener("readystatechange", function (e) {
        var respondURL = Sdk.Common().detectInternetExplorer() ? this._url : this.responseURL;
        if (!_.isUndefined(respondURL)) {
          if (this.status == 200 && (respondURL.match(/sections=cart/gi)) && this.readyState == 4) {
            if (this.responseText && this.responseText.length > 0) {
              api.get(__cnStore.STORE_GET_CART_MAGENTO+'?i='+Math.floor((Math.random() * 9999) + 1000)).then((response) => {
                window.stateXhr++;
                synchronizeCart(response.responseText);
              });
            }
          }
        }
      }, false);
      open.apply(this, arguments);
    };
  })(XMLHttpRequest.prototype.open);
});