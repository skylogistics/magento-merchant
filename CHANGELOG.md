# [1.0.4](https://bitbucket.org/skylogistics/magento-merchant) (2018-07-24)

### Features
* **Add:** Basic code for type 3 integration. ([3b026a1](https://bitbucket.org/skylogistics/magento-merchant/commits/3b026a15438161c7797f06f26992af4df53150f9))

* **Update:** Basic code for type 1 integration. ([a5596ae](https://bitbucket.org/skylogistics/magento-merchant/commits/a5596aeccda23d6ad48f7c679af242b8278cb8d0))


# [1.0.3](https://bitbucket.org/skylogistics/magento-merchant) (2018-05-04)

### Features

* **references:** Stable version ([16a9fa1](https://bitbucket.org/skylogistics/magento-merchant/commits/16a9fa1716df962d765f554f0cb45e0201d2a642))