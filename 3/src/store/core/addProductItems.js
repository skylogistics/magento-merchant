const _ = require('underscore');
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]
};
const sessionStore = engine.createStore(storage.session, []);
const localStore = engine.createStore(storage.local, []);

// Local
const __cnStore = require('../../../config/store.json');

export function addProductItems(products) {
  var ListProducts = [];

  products.forEach(function (productItem) {
    let title = productItem.product_title;

    if (__cnStore.CONF.PRODUCT_TYPE_DEFAULT !== '') {
      productItem.product_type = __cnStore.CONF.PRODUCT_TYPE_DEFAULT;
    } else {
      productItem.product_type = productItem.category;
    }

    let _Optionals = {
      CustomFields: [
        String(productItem.sku)
      ]
    }

    var storeConfiguration = localStore.get('skb-cfg');
    
    let Product = {
      HtmlObjectid: String(productItem.quote_id),
      Id: 0,
      Sku: productItem.quote_id,
      Name: title + ' ' + productItem.variant_title,
      Category: productItem.product_type,
      Price: productItem.price,
      ImgUrl: productItem.image,
      Language: "",
      Weight: productItem.weight ? productItem.weight : 0,
      WeightUnit: storeConfiguration.weightUnit,
      VolumetricWeight: 0,
      DefinitionOpt: "",
      Quantity: productItem.quantity,
      ProductMerchantId: productItem.variant_id,
      VariantMerchantId: productItem.variant_id
    }
    _Optionals.Attributes = productItem.options;
    ListProducts.push(
      {
        "Product": Product,
        "Optionals": _Optionals
      }
    );

  });
  return { "ListProducts": ListProducts };
}