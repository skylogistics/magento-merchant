const _ = require('underscore');
const tingle = require('tingle.js');

// Storage
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]
};
const localStore = engine.createStore(storage.local, []);
const sessionStore = engine.createStore(storage.session, []);
// Local
const api = require('../../xhr');
const __cnStore = require('../../../config/store.json');

import { addProductItems } from './addProductItems.js';
import { validateRestriction } from './validateRestriction.js';

export function synchronizeCart(data) {
  if (data.responseText && data.responseText.length > 0) {
    var itemsStore = JSON.parse(data.responseText);

    Sdk.getLocationAllow().then(function () {
      if (itemsStore.length > 0) {
        var loaderModal = new tingle.modal({
          footer: false,
          stickyFooter: false,
          closeMethods: []
        });
        
        loaderModal.setContent('<center><span> Wait a moment please </span><div style="padding-top: 10px;"><img src="https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderBlackBlue.gif"  /></div></center>');
        loaderModal.open();

        var restrictedModal = new tingle.modal({
          footer: true,
          stickyFooter: true,
          closeMethods: ['overlay', 'button', 'escape'],
          closeLabel: "Close",
          onClose: function () {
            setTimeout(() => {
              top.location.reload();    
            }, 300);  
          }
        });

        var prod = addProductItems(itemsStore);

        Sdk.addProductsCart(prod).then(function (response) {
          var productsResponse = response.Data ? response.Data.ListProducts : [];
          var arrayRemove = _.filter(productsResponse, function (prod) {
            var success = prod.Success;
            return success === false;
          });

          if (arrayRemove.length < 1) {
            Sdk.getCartRefresh().then(function (Cart2) {
              sessionStore.set('cart_prod_arr', []);
              sessionStore.set('cart_prod_arr', Cart2);
              Sdk.Common().changeLastCountry(sessionStore.get('auth-store').Data.CART_SKY.Country.Name);
              var pOptional = {
                IdCartExternal: "" ,
                IdStoreExternal: localStore.get('skb-cfg').IdStore
              }   
              loaderModal.close();               
              Sdk.showCheckoutPageV2(pOptional);
            });
          }
          else {
            var currentCountry = sessionStore.get('auth-store').Data.CART_SKY.Country.Name;
            var validate = validateRestriction(arrayRemove, itemsStore, currentCountry),
                update = validate.objUpdate,
                html = validate.html;

            if (Object.keys(update).length > 0) {
              let idV = [];

              for (let i = 0; i < arrayRemove.length; i++) {
                if (arrayRemove[i].Errors.length > 0) {
                  itemsStore.map(function(item) {
                    if (item.quote_id == arrayRemove[i].HtmlObjectId) {
                      idV.push({
                          idVariant: item.variant_id,                        
                          Errors: arrayRemove[i].Errors
                      });
                    }
                  });
                }
              }

              api.get(__cnStore.STORE_GET_CART_MAGENTO + 'delete?idVariant=' + JSON.stringify(idV)).then(function () {
                if (html.length > 0) {
                  restrictedModal.setContent(html);
                  restrictedModal.addFooterBtn('Close', 'tingle-btn tingle-btn--default tingle-btn--pull-right', function () {
                    restrictedModal.close();
                  });
                }

                Sdk.getCartRefresh().then(function (Cart2) {
                  sessionStore.set('cart_prod_arr', []);
                  sessionStore.set('cart_prod_arr', Cart2);
                  Sdk.Common().changeLastCountry(sessionStore.get('auth-store').Data.CART_SKY.Country.Name);
                  var pOptional = {
                    IdCartExternal: "" ,
                    IdStoreExternal: localStore.get('skb-cfg').IdStore
                  }   
                  Sdk.showCheckoutPageV2(pOptional);
                  loaderModal.close();
                  restrictedModal.open();
                });
              });
            }
          }
        });
      }
    }).catch(function (error) {
      console.log(':: getLocationAllow error ', error);
    });
  }
}
