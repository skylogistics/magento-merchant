import { setTimeout } from 'timers';
import SkyboxSDK from '@skyboxcheckout/merchant-sdk';
import { synchronizeCart } from './core/synchronizeCart.js';

const _ = require('underscore');
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]      
};    
const localStore = engine.createStore(storage.local, []);
const sessionStore = engine.createStore(storage.session, []);
// Local libs
const magentoStyles = require('../../assets/css/magento.css');
const __cnStore = require('../../config/store.json');
const api = require('../xhr');

let conf = localStore.get('skb-cfg');

if ( _.isObject(conf)) {
  __cnStore.IDSTORE = conf.merchantId;
  __cnStore.MERCHANTCODE = conf.merchantCode;
  __cnStore.MERCHANT = conf.merchantKey;
  __cnStore.SUCCESSFUL_PAGE = conf.successUrl;
  __cnStore.CHECKOUT_PAGE = conf.checkoutUrl;
  __cnStore.STORE_GET_CART_MAGENTO = conf.cartUrl;

  if (!_.isNull(conf.weightUnit) && !_.isUndefined(conf.weightUnit) && conf.weightUnit !== "") {
    window.Sdk = new SkyboxSDK({
      IDSTORE: __cnStore.IDSTORE,
      MERCHANT: __cnStore.MERCHANT,
      MERCHANTCODE: __cnStore.MERCHANTCODE,
      STORE_URL: __cnStore.STORE_URL,
      SUCCESSFUL_PAGE: __cnStore.SUCCESSFUL_PAGE,
      CHECKOUT_PAGE: __cnStore.CHECKOUT_PAGE,
      CHECKOUT_BUTTON_CLASS: __cnStore.CHECKOUT_BUTTON_CLASS
    });
  }
}

$(document).ready(function () {
  Sdk.Common().initBtnSkyCheckout();

  // Skybox Ckeckout page & Iframe Source
  if ($(location).attr('href').indexOf(__cnStore.CHECKOUT_PAGE) > -1 &&  $(location).attr('href').indexOf(__cnStore.SUCCESSFUL_PAGE) == -1) {
    $("#skybox-checkout-change-country").css('display', 'none');
    /* hide mini cart icon
      $('.minicart-wrapper').hide(); 
    */
    api.get(__cnStore.STORE_GET_CART_MAGENTO+'?i='+Math.floor((Math.random() * 9999) + 1000)).then((cart) => {
      let data = JSON.parse(cart.responseText);

      if (data.length === 0) {
        localStore.remove('mage-cache-storage');
        window.history.back();                
      }

      Sdk.deleteProductsCart().then(function () {
        synchronizeCart(cart);
      });
    });
  }

  // Skybox Ckeckout Successfull page & Invoice Source
  if ($(location).attr('href').indexOf(__cnStore.SUCCESSFUL_PAGE) > -1) {
    /* hide mini cart icon
      $('.minicart-wrapper').hide(); 
    */
    $("#skybox-checkout-change-country").css('display', 'none');

    if ($('#skybox-international-checkout-invoice').length > 0) {
      let rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderGris.gif";

      if (__cnStore.SKBX_LOADER_NAME) {
        rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/" + __cnStore.SKBX_LOADER_NAME;
      }

      $('#skybox-international-checkout-invoice').html(`
        <h1 id="mensaje" style="text-align:center;">
          <img src="${rutaLoaderGif}"/>
        </h1>
      `);
      
      api.get(__cnStore.STORE_GET_CART_MAGENTO + '/clear').then(() => {
        localStore.remove('mage-cache-storage');

        Sdk.getCartInvoice().then(function (content) {
          var content = JSON.parse(content).Data.Invoice
          var invoice = document.getElementById('skybox-international-checkout-invoice');
          invoice.innerHTML = content;
        });
      });
    }
  };
});
